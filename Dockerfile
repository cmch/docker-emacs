FROM debian:10-slim

RUN \
apt update && apt install -y  \
git \
openssh-client \
emacs25 \
aspell
